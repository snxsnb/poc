import { getDistance } from 'geolib';
import { getCurrentLocation } from './LocationService';
import { db } from '../config/firebase';

export async function updateOrCreateUser({ uid, email, name, about, location }) {
  const user = db.ref(`users/${uid}`);
  if ((await user.once('value')).exists()) {
    email && await user.child('info').child('email').set(about);
    name && await user.child('info').child('name').set(name);
    about && await user.child('info').child('about').set(about);
    location && await user.child('location').set(location);
  } else {
    await user.set({
      info: { name, email, about },
      ...(location && { location }),
    });
  }

  return (await user.once('value')).val();
}

export async function getNearestUsers(uid, location, radius) {
  const { accuracy: currentAccuracy, ...currentLocation } = location;
  const users = [];

  (await db.ref('users').once('value'))
    .forEach((user) => {
      if (user.key !== uid) {
        const { location, info } = user.val();
        if (!location) {
          return;
        }

        const distance = getDistance(currentLocation, location);
        const optimisticDistance = distance - currentAccuracy;
        if (optimisticDistance <= radius) {
          users.push({
            uid: user.key,
            distance: (optimisticDistance >= 0 && optimisticDistance) || 0,
            ...info,
          });
        }
      }
    });

  return users;
}

export async function getUserInfo(uid) {
  const user = await db.ref(`users/${uid}`).once('value');
  return user.child('info').val();
}

export async function getUser(uid) {
  const user = await db.ref(`users/${uid}`).once('value');
  return user.val();
}

export async function updateLocation(uid) {
  const location = getCurrentLocation();
  return updateOrCreateUser({ uid, location });
}
