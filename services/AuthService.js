import { auth } from '../config/firebase';
import { updateOrCreateUser } from './UserService';

export async function signIn(email, password) {
  return new Promise((resolve) => {
    auth
      .signInWithEmailAndPassword(email, password)
      .then(({ user: { uid } }) => resolve({ uid }))
      .catch(({ message }) => resolve({ error: message }));
  });
}

export async function signUp({ email, password, name, about }) {
  const { uid, error } = await (new Promise((resolve) => {
    auth
      .createUserWithEmailAndPassword(email, password)
      .then(({ user: { uid: userUid } }) => resolve({ uid: userUid }))
      .catch(({ message }) => resolve({ error: message }));
  }));

  if (error) {
    return { error };
  }

  await updateOrCreateUser({ uid, email, name, about });
  return { uid };
}
