import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { Text, View } from 'react-native';

import styles from './styles';

const propTypes = {
  data: PropTypes.object.isRequired,
};

function Profile({
  data: {
    name,
    about,
  },
}) {
  return (
    <View style={styles.profile}>
      <Text style={styles.title}>
        Name:
      </Text>
      <Text style={styles.name}>
        {name}
      </Text>
      <Text style={styles.title}>
        About:
      </Text>
      <Text style={styles.about}>
        {about}
      </Text>
    </View>
  );
}

Profile.propTypes = propTypes;
Profile.navigationOptions = {
  title: 'Profile',
};

export default compose(
  connect(({ selectedResult }) => ({ data: selectedResult })),
)(Profile);
