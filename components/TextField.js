import React from 'react';
import PropTypes from 'prop-types';
import { Text, View, TextInput, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  root: {
    paddingTop: 10,
    paddingBottom: 10,
  },
  label: {
    textAlign: 'center',
    fontSize: 20,
    color: '#2C2C2C',
  },
  input: {
    minWidth: 150,
    textAlign: 'center',
    fontSize: 16,
  },
});

const propTypes = {
  label: PropTypes.string.isRequired,
  value: PropTypes.string,
  setValue: PropTypes.func.isRequired,
};

const defaultProps = {
  value: '',
};

function TextField({ label, value, setValue, ...inputProps }) {
  return (
    <View style={styles.root}>
      <Text style={styles.label}>
        {`${label}:`}
      </Text>
      <TextInput
        style={styles.input}
        onChangeText={value => setValue(value)}
        value={value}
        {...inputProps}
      />
    </View>
  );
}

TextField.propTypes = propTypes;
TextField.defaultProps = defaultProps;

export default TextField;
