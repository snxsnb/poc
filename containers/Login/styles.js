import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
  },
  loginContainer: {
    flex: 1,
    justifyContent: 'center',
    padding: 20,
    backgroundColor: '#fff',
  },
  error: {
    marginTop: 10,
    textAlign: 'center',
    fontSize: 16,
    color: 'red',
  },
  modeSwitch: {
    width: 150,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignSelf: 'center',
  },
});
