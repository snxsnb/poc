import React from 'react';
import { AppState } from 'react-native';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import reducer from './reducers';
import { startLocationWatch, stopLocationWatch } from './actions';

import Main from './containers/Main';

const store = createStore(
  reducer,
  applyMiddleware(thunk),
);

AppState.addEventListener('change', async (appState) => {
  if (appState === 'active') {
    await store.dispatch(startLocationWatch());
  }

  if (appState === 'inactive' || appState === 'background') {
    await store.dispatch(stopLocationWatch());
  }
});

function App() {
  return (
    <Provider store={store}>
      <Main />
    </Provider>
  );
}

export default App;
