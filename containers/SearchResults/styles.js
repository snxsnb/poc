import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  results: {
  },
  resultItem: {
    height: 100,
    padding: 10,
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: 'lightgray',
  },
  resultItemName: {
    fontSize: 30,
  },
  resultItemDistance: {
    fontSize: 15,
  },
});
