import orderBy from 'lodash/orderBy';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { Text, ScrollView, TouchableOpacity } from 'react-native';

import Button from '../../components/Button';
import { selectResult, extendSearch } from '../../actions';
import styles from './styles';

const propTypes = {
  navigation: PropTypes.object.isRequired,
  results: PropTypes.array.isRequired,
  selectResult: PropTypes.func.isRequired,
  extendSearch: PropTypes.func.isRequired,
};

export function SearchResults({
  navigation: { navigate },
  results,
  selectResult,
  extendSearch,
}) {
  const orderedResults = orderBy(results, ['distance'], ['asc']);
  return (
    <ScrollView style={styles.results}>
      {orderedResults.map(({ uid, name, distance }) => (
        <TouchableOpacity
          style={styles.resultItem}
          key={uid}
          onPress={() => selectResult(uid, () => navigate('Profile'))}
        >
          <Text style={styles.resultItemName}>
            {name}
          </Text>
          <Text style={styles.resultItemDistance}>
            {`~${distance.toFixed()} meters away`}
          </Text>
        </TouchableOpacity>
      ))}
      <Button
        title="+50 meters"
        onPress={() => extendSearch(50)}
      />
    </ScrollView>
  );
}

SearchResults.propTypes = propTypes;
SearchResults.navigationOptions = {
  title: 'Search results',
};

export default compose(
  connect(
    ({ searchResults }) => ({ results: searchResults }),
    { selectResult, extendSearch },
  ),
)(SearchResults);
