import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, withState } from 'recompose';
import { View } from 'react-native';

import TextField from '../../components/TextField';
import Button from '../../components/Button';
import { signUp } from '../../actions';
import styles from './styles';

const propTypes = {
  navigate: PropTypes.func.isRequired,
  signUp: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  setEmail: PropTypes.func.isRequired,
  password: PropTypes.string.isRequired,
  setPassword: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  setName: PropTypes.func.isRequired,
  about: PropTypes.string.isRequired,
  setAbout: PropTypes.func.isRequired,
};

function SignUp({
  navigate,
  signUp,
  email,
  setEmail,
  password,
  setPassword,
  name,
  setName,
  about,
  setAbout,
}) {
  return (
    <View style={styles.container}>
      <TextField
        label="Email"
        value={email}
        setValue={setEmail}
      />
      <TextField
        label="Password"
        value={password}
        setValue={setPassword}
        secureTextEntry
      />
      <TextField
        label="Name"
        value={name}
        setValue={setName}
      />
      <TextField
        label="About"
        value={about}
        setValue={setAbout}
      />
      <Button
        title="Sign up"
        onPress={() => signUp({ email, password, name, about }, () => navigate('Home'))}
      />
    </View>
  );
}

SignUp.propTypes = propTypes;

export default compose(
  withState('email', 'setEmail', ''),
  withState('password', 'setPassword', ''),
  withState('name', 'setName', ''),
  withState('about', 'setAbout', ''),
  connect(null, { signUp }),
)(SignUp);
