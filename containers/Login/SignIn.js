import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, withState } from 'recompose';
import { View } from 'react-native';

import TextField from '../../components/TextField';
import Button from '../../components/Button';
import { signIn } from '../../actions';
import styles from './styles';

const propTypes = {
  navigate: PropTypes.func.isRequired,
  signIn: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired,
  setEmail: PropTypes.func.isRequired,
  password: PropTypes.string.isRequired,
  setPassword: PropTypes.func.isRequired,
};

function SignIn({ navigate, signIn, email, setEmail, password, setPassword }) {
  return (
    <View style={styles.container}>
      <TextField
        label="Email"
        value={email}
        setValue={setEmail}
      />
      <TextField
        label="Password"
        value={password}
        setValue={setPassword}
        secureTextEntry
      />
      <Button
        title="Sign in"
        onPress={() => signIn(email, password, () => navigate('Home'))}
      />
    </View>
  );
}

SignIn.propTypes = propTypes;

export default compose(
  withState('email', 'setEmail', ''),
  withState('password', 'setPassword', ''),
  connect(null, { signIn }),
)(SignIn);
