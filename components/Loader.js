import React from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
  },
});

function Loader({ ...props }) {
  return (
    <View style={styles.container}>
      <ActivityIndicator
        size="large"
        color="#2C2C2C"
        {...props}
      />
    </View>
  );
}

export default Loader;
