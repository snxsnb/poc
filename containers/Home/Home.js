import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, withState } from 'recompose';
import { Text, View, TouchableOpacity, Slider } from 'react-native';

import { search } from '../../actions';
import styles from './styles';

const LOCATION_ERROR_MESSAGE = 'Your location is unavailable, try again later.';

const propTypes = {
  navigation: PropTypes.object.isRequired,
  radius: PropTypes.number.isRequired,
  setRadius: PropTypes.func.isRequired,
  search: PropTypes.func.isRequired,
};

function Home({
  navigation: { navigate },
  radius,
  setRadius,
  search,
}) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.searchButton}
        onPress={() => search(
          radius,
          () => navigate('SearchResults'),
          () => alert(LOCATION_ERROR_MESSAGE),
        )}
      >
        <Text style={styles.searchLabel}>search</Text>
      </TouchableOpacity>
      <Text style={styles.inputLabel}>Search radius:</Text>
      <Text style={styles.radiusLabel}>{`${radius} meters`}</Text>
      <Slider
        style={styles.radiusSlider}
        onValueChange={value => setRadius(value)}
        value={radius}
        step={10}
        minimumValue={10}
        maximumValue={1000}
      />
    </View>
  );
}

Home.propTypes = propTypes;
Home.navigationOptions = {
  title: 'Profile',
};

export default compose(
  withState('radius', 'setRadius', 150),
  connect(null, { search }),
)(Home);
