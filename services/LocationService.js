const { geolocation } = navigator; // eslint-disable-line
const showMessage = alert; // eslint-disable-line

const LOCATION_TIMEOUT = 60000;
const LOCATION_CACHE_AGE = 20000;

export async function getCurrentLocation() {
  return new Promise(
    resolve => geolocation.getCurrentPosition(
      ({ coords: { latitude, longitude, accuracy } }) => resolve({ latitude, longitude, accuracy }),
      ({ message }) => { showMessage(message); resolve(); },
      {
        timeout: LOCATION_TIMEOUT,
        maximumAge: LOCATION_CACHE_AGE,
        enableHighAccuracy: true,
      },
    ),
  );
}

export function startWatch(successHandler, errorHandler) {
  return geolocation.watchPosition(
    ({ coords: { latitude, longitude, accuracy }, timestamp }) => {
      successHandler({
        latitude,
        longitude,
        accuracy,
        date: new Date(timestamp).toLocaleString(),
      });
    },
    ({ message }) => {
      (errorHandler && errorHandler()) || showMessage(message);
    },
    {
      timeout: LOCATION_TIMEOUT,
      maximumAge: LOCATION_CACHE_AGE,
      enableHighAccuracy: true,
      distanceFilter: 0,
    },
  );
}

export function stopWatch(watchId) {
  geolocation.clearWatch(watchId);
}
