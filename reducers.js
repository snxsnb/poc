import { ActionTypes } from './actions';

const initialState = {
  auth: {},
  user: {},
  loader: {
    loading: false,
  },
  searchResults: [],
  selectedResult: {},
  internal: {},
};

export default function (state = initialState, { type, payload }) {
  switch (type) {
    case ActionTypes.START_LOADER: {
      return {
        ...state,
        loader: { loading: true },
      };
    }
    case ActionTypes.STOP_LOADER: {
      return {
        ...state,
        loader: { loading: false },
      };
    }
    case ActionTypes.SIGN_IN:
    case ActionTypes.SIGN_UP: {
      return {
        ...state,
        auth: payload,
      };
    }
    case ActionTypes.SEARCH:
    case ActionTypes.EXTEND_SEARCH: {
      return {
        ...state,
        ...payload,
      };
    }
    case ActionTypes.SELECT_RESULT: {
      return {
        ...state,
        selectedResult: payload,
      };
    }
    case ActionTypes.UPDATE_LOCATION: {
      return {
        ...state,
        user: {
          ...state.user,
          location: payload,
        },
      };
    }
    case ActionTypes.START_LOCATION_WATCH: {
      return {
        ...state,
        internal: {
          ...state.internal,
          locationWatchId: payload,
        },
      };
    }
    case ActionTypes.STOP_LOCATION_WATCH: {
      const { internal: { locationWatchId, ...internal } } = state;
      return {
        ...state,
        internal,
      };
    }
    default:
      return state;
  }
}
