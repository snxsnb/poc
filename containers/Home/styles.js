import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchButton: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 150,
    height: 150,
    borderWidth: 7,
    borderColor: '#2C2C2C',
    borderRadius: 75,
  },
  searchLabel: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#2C2C2C',
  },
  inputLabel: {
    marginTop: 10,
    fontSize: 20,
    color: '#2C2C2C',
  },
  radiusSlider: {
    width: 200,
    marginTop: 10,
  },
  radiusLabel: {
    marginTop: 10,
    fontSize: 16,
  },
});
