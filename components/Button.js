import React from 'react';
import { Button } from 'react-native';

function WrappedButton({ ...props }) {
  return (
    <Button
      color="#2C2C2C"
      {...props}
    />
  );
}

export default WrappedButton;
