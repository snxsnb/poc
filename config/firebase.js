import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyCWQqjH7kQmK1bU9uMpErfhxoea7yNJ8VA',
  authDomain: 'base1-55b6e.firebaseapp.com',
  databaseURL: 'https://base1-55b6e.firebaseio.com',
  projectId: 'base1-55b6e',
  storageBucket: 'base1-55b6e.appspot.com',
  messagingSenderId: '903191122998',
};
firebase.initializeApp(config);

export const db = firebase.database();
export const auth = firebase.auth();
