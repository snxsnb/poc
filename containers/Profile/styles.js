import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  profile: {
    flex: 1,
    padding: 10,
    backgroundColor: '#fff',
  },
  name: {
    fontSize: 20,
  },
  about: {
    fontSize: 20,
  },
  title: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
