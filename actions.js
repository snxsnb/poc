import find from 'lodash/find';
import isUndefined from 'lodash/isUndefined';
import * as AuthService from './services/AuthService';
import * as UserService from './services/UserService';
import * as LocationService from './services/LocationService';

export const START_LOADER = 'START_LOADER';
export function startLoader() {
  return { type: START_LOADER };
}

export const STOP_LOADER = 'STOP_LOADER';
export function stopLoader() {
  return { type: STOP_LOADER };
}

export const SIGN_IN = 'SIGN_IN';
export function signIn(email, password, successAction) {
  return async function (dispatch) {
    await dispatch({ type: START_LOADER });
    const result = await AuthService.signIn(email, password);
    await dispatch({ type: SIGN_IN, payload: result });
    if (!result.error) {
      await successAction();
    }
    await dispatch({ type: STOP_LOADER });
  };
}

export const SIGN_UP = 'SIGN_UP';
export function signUp({ email, password, name, about }, successAction) {
  return async function (dispatch) {
    await dispatch({ type: START_LOADER });
    const result = await AuthService.signUp({ email, password, name, about });
    await dispatch({ type: SIGN_UP, payload: result });
    if (!result.error) {
      await successAction();
    }
    await dispatch({ type: STOP_LOADER });
  };
}

export const UPDATE_LOCATION = 'UPDATE_LOCATION';
export function updateLocation(location) {
  return async function (dispatch, getState) {
    const { auth: { uid } } = getState();
    if (uid) {
      await UserService.updateOrCreateUser({ uid, location });
    }

    await dispatch({ type: UPDATE_LOCATION, payload: location });
  };
}

export const START_LOCATION_WATCH = 'START_LOCATION_WATCH';
export function startLocationWatch() {
  return async function (dispatch) {
    const watchId = LocationService.startWatch(
      data => dispatch(updateLocation(data)),
    );
    await dispatch({ type: START_LOCATION_WATCH, payload: watchId });
  };
}

export const STOP_LOCATION_WATCH = 'STOP_LOCATION_WATCH';
export function stopLocationWatch() {
  return async function (dispatch, getState) {
    const { internal: { locationWatchId } } = getState();
    if (!isUndefined(locationWatchId)) {
      LocationService.stopWatch(locationWatchId);
    }

    await dispatch({ type: STOP_LOCATION_WATCH });
  };
}

export const SEARCH = 'SEARCH';
export function search(radius, successAction, failAction) {
  return async function (dispatch, getState) {
    await dispatch({ type: START_LOADER });
    const { auth: { uid }, user: { location } } = getState();
    let users = [];
    if (uid && location) {
      users = await UserService.getNearestUsers(uid, location, radius);
      successAction();
    } else {
      failAction();
    }

    await dispatch({ type: SEARCH, payload: { searchResults: users, searchRadius: radius } });
    await dispatch({ type: STOP_LOADER });
  };
}

export const SELECT_RESULT = 'SELECT_RESULT';
export function selectResult(uid, successAction) {
  return async function (dispatch, getState) {
    const { searchResults } = getState();
    const selectedResult = find(searchResults, { uid });
    await dispatch({ type: SELECT_RESULT, payload: selectedResult });
    successAction();
  };
}

export const EXTEND_SEARCH = 'EXTEND_SEARCH';
export function extendSearch(extension) {
  return async function (dispatch, getState) {
    const {
      auth: { uid },
      user: {
        location,
      },
      searchRadius,
    } = getState();
    const extendedRadius = searchRadius + extension;
    await dispatch({ type: START_LOADER });
    const users = await UserService.getNearestUsers(uid, location, extendedRadius);
    await dispatch({
      type: EXTEND_SEARCH,
      payload: { searchResults: users, searchRadius: extendedRadius },
    });
    await dispatch({ type: STOP_LOADER });
  };
}

export const ActionTypes = {
  START_LOADER,
  STOP_LOADER,
  SEARCH,
  SIGN_IN,
  SIGN_UP,
  UPDATE_LOCATION,
  START_LOCATION_WATCH,
  STOP_LOCATION_WATCH,
  SELECT_RESULT,
  EXTEND_SEARCH,
};
