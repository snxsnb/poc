import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'recompose';
import { View, StyleSheet } from 'react-native';
import { createSwitchNavigator, createStackNavigator } from 'react-navigation';

import Login from '../Login';
import Home from '../Home';
import SearchResults from '../SearchResults';
import Profile from '../Profile';
import Loader from '../../components/Loader';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mainContainer: {
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  hidden: {
    display: 'none',
  },
});

const AppStack = createStackNavigator({
  Home: {
    screen: Home,
    navigationOptions: () => ({
      header: null,
    }),
  },
  SearchResults,
  Profile,
});

const MainStack = createSwitchNavigator(
  {
    App: AppStack,
    Login,
  },
  {
    initialRouteName: 'Login',
  },
);

const propTypes = {
  loading: PropTypes.bool.isRequired,
};

function Main({ loading }) {
  return (
    <View style={[styles.container, styles.mainContainer]}>
      {loading && <Loader />}
      <View style={[styles.container, (loading && styles.hidden)]}>
        <MainStack />
      </View>
    </View>
  );
}

Main.propTypes = propTypes;

export default compose(
  connect(({ loader: { loading } }) => ({ loading })),
)(Main);
