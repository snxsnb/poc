import _ from 'lodash';
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose, withState } from 'recompose';
import { ScrollView, View, Text } from 'react-native';

import Button from '../../components/Button';
import SignIn from './SignIn';
import SignUp from './SignUp';
import styles from './styles';

const MODES = {
  SIGN_IN: 'SIGN_IN',
  SIGN_UP: 'SIGN_UP',
};

const propTypes = {
  navigation: PropTypes.object.isRequired,
  mode: PropTypes.oneOf(_.values(MODES)).isRequired,
  setMode: PropTypes.func.isRequired,
  error: PropTypes.string,
};

const defaultProps = {
  error: '',
};

function Login({
  navigation: { navigate },
  mode,
  setMode,
  error,
}) {
  return (
    <ScrollView contentContainerStyle={styles.loginContainer}>
      <View style={styles.modeSwitch}>
        <Button
          title="Sign in"
          onPress={() => setMode(MODES.SIGN_IN)}
        />
        <Button
          title="Sign up"
          onPress={() => setMode(MODES.SIGN_UP)}
        />
      </View>
      {!!error && (
        <Text style={styles.error}>
          {error}
        </Text>
      )}
      {(mode === MODES.SIGN_IN && <SignIn {...{ navigate }} />)
        || (mode === MODES.SIGN_UP && <SignUp {...{ navigate }} />)}
    </ScrollView>
  );
}

Login.propTypes = propTypes;
Login.defaultProps = defaultProps;
Login.navigationOptions = {
  title: 'Login',
};

export default compose(
  withState('mode', 'setMode', MODES.SIGN_IN),
  connect(({ auth: { error } }) => ({ error })),
)(Login);
